# Where to put configs?

| file | folder |
|---|---|
| `bspwm` | `~/.config/bspwm` |
| `sxhkd` | `~/.config/sxhkd` |
| `.alacritty.yml` | `~/` |
| `.bashrc` | `~/` |
| `.zshrc` | `~/` |
| `.gitconfig` | `~/` |
| `.gnuplot` | `~/` |
| `.tmux.conf` | `~/` |
| `.vimrc` | `~/` |

# Setup Notes

## Bspwm

### How to setup bspwm

- Install via package manager.

- Configs go into
  - `~/.config/bspwm/bspwmrc`
  - `~/.config/sxhkd/sxhkdrc`

- Optional configs
  - `~/.config/polybar/config`
  - `~/.config/polybar/launch.sh`

- Make sure that `bspwmrc` does not throw errors, e.g. all programms called in there are
  also installed.

### Issues

- Once `bspwm` broke upon a `dnf update`. For several reasosn I then reinstalled the whole OS,
again using Fedora 36. But this did not fix it either.
After a while I used GNOME but some day I came back and could get it to work again
by installing all the dependencies listed at https://github.com/baskerville/bspwm/wiki#from-source.
Perhaps if that happens again in a fresh install I should report that problem and fix somewhere,
maybe at
- https://ask.fedoraproject.org/t/bspwm-black-screen-upon-execution/27628
- https://src.fedoraproject.org/rpms/bspwm
- The "Large Text" option in `gnome-control-setting` might not work with `bspwm`.
  Instead configure the `~/.Xresources` file. This also allows for fractional scaling
  (only for fonts), see https://wiki.archlinux.org/title/HiDPI.

## Capslock

- Fedora [GNOME]
  - Install gnome-tweaks: `dnf install gnome-tweaks`
  - Tweaks -> Keyboard -> Additional Layout Options -> Capslock
- Fedora [BSPWM]
  - Copy `config_fwork/X11/xorg.conf.d/00-keyboard.conf` manually to
    `/etc/X11/xorg.conf.d/` (needs sudo).
  - [Outdated] Use `setxkbmap`:
    ```
    setxkbmap -option 'caps:none'
    setxkbmap -option 'caps:ctrl_modifier'
    ```

## Intel graphics driver on framework laptop and Fedora 36

Follow instructions here:
https://discussion.fedoraproject.org/t/intel-graphics-best-practices-and-settings-for-hardware-acceleration/69944

- Add `LIBVA_DRIVER_NAME=iHD` to `.zshrc`.
- Reboot after installation in case `vainfo` gives errors.

Note: There might be conflicts with `ffmpeg`.
See this for a solution: https://discussion.fedoraproject.org/t/issues-installing-ffmpeg-fedora-39/99442/2
So far I used the `--allow-erasing` one.

# JuliaMono font

- Download from https://github.com/cormullion/juliamono
- Move to `~/.fonts` folder.
- Run `fc-cache -f -v`.

## Oh-my-Zsh

```
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
```

## Polybar

- Docs at [](https://github.com/polybar/polybar/wiki)

- Make sure relevant fonts are installed.

- The logs provide useful information if something is not working,
  see `launch.sh` for the log filename.

### Backlight control

- don't use `xbacklight`, because it seems to be outdated: https://gitlab.com/wavexx/acpilight
- We use `backlight` instead which directly interacts with the backlight
  through `/sys/class/backlight/`.
  For this to work we follow the instructions in the readme of `acpilight`.
  - Add user to `video` group: `sudo usermod -a -G video <user>`
  - Add a `udev` rule that enables members of `video` group to write to the `backlight` file.
  For `fwork` add the following to `/etc/udev/rules.d/backlight.rules`:
  ```
  ACTION=="add", SUBSYSTEM=="backlight", RUN+="/bin/chgrp video /sys/class/backlight/intel_backlight/brightness"
  ACTION=="add", SUBSYSTEM=="backlight", RUN+="/bin/chmod g+w /sys/class/backlight/intel_backlight/brightness"
  ```
  - Apply `udev` rules by either rebooting or
  ```sh
  udevadm control --reload-rules && udevadm trigger
  ```
  See also Arch wiki for details of the method: https://wiki.archlinux.org/title/Backlight

## Rofi

- Use `rofi-theme-selector` to preview themes.

- On Fedora, somehow, themes are only read from
  `/usr/share/rofi/themes`, although it should
  also read from `~/.config/rofi/themes` or
  `~/.local/share/rofi/themes`.

- `sidebar by Qball` is the best :)

## Wallpaper

- Need to build `xwallpaper` from source on Fedora.
- Some dependencies: `libxcb, xcb-util, xcb-utils-image`
- Make sure that `libjpeg-turbo-devel, libpng-devel, xrandr, xcb-util-image-devel, xcb-util-devel`
  dependencies are installed, needed to display all image format.
- Check `configure` output for other dependencies in case its still not working.

## Tmux

- Install TPM `git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm`
- Install plugins `Ctrl-B  + I`

## Vim

- Install Vundle `git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim`

## Font scaling

Source: https://wiki.archlinux.org/title/HiDPI - Text Scaling
Install `gnome-tweaks` and go to Fonts -> Scaling factor to change size of UI text.

Alternatively, use
`gsettings set org.gnome.desktop.interface text-scaling-factor 1.5`
