let $vimhome=fnamemodify(resolve(expand("~/")),':p:h')
let $vundle=$vimhome."/.vim/bundle/Vundle.vim"

" Be iMproved
set nocompatible

let g:polyglot_disabled = ['csv']

"=====================================================
"" Vundle settings
"=====================================================
filetype off
set rtp+=$vundle
call vundle#begin()
Plugin 'VundleVim/Vundle.vim'               " let Vundle manage Vundle, required
Plugin 'bling/vim-airline'                  " Lean & mean status/tabline for vim
Plugin 'dikiaap/minimalist'                 " minimalist airline theme
Plugin 'scrooloose/nerdcommenter'           " Commenter plugin
Plugin 'sheerun/vim-polyglot'               " syntax highlithing
Plugin 'dhruvasagar/vim-zoom'               " C-m to toggle split zooms
Plugin 'rakr/vim-one'                       " colorscheme
Plugin 'christoomey/vim-tmux-navigator'     " interaction with tmux
Plugin 'SirVer/ultisnips'                   " snippet engine
Plugin 'honza/vim-snippets'                 " snippets for ultisnips
Plugin 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plugin 'junegunn/fzf.vim'                   " fuzzy finder
Plugin 'lervag/vimtex'                      " LaTeX commands
Plugin 'tpope/vim-speeddating'              " increment dates properly
Plugin 'JuliaEditorSupport/julia-vim'       " converts utf8 symbols in .jl files
Plugin 'tpope/vim-unimpaired'               " useful bindings (leader [ or ])
Plugin 'tpope/vim-repeat'                   " extend . functionality
Plugin 'inkarkat/vim-visualrepeat'          " let . repeat visual blocks
Plugin 'tpope/vim-surround'                 " surround motions
Plugin 'tpope/vim-sleuth'                   " automatic identent settings detection
Plugin 'jkramer/vim-checkbox'               " checkbox ticking support
Plugin 'justinmk/vim-sneak'                 " faster motions
Plugin 'derekwyatt/vim-fswitch'             " switch between c/c++ headers
Plugin 'jpalardy/vim-slime'                 " send commands to tmux panes
Plugin 'junegunn/vim-easy-align'            " alignment helper
call vundle#end()                           " required

"=====================================================
"" General settings
"=====================================================
syntax enable                               " syntax highlight

set t_Co=256                                " set 256 colors
colorscheme one                     " set colorscheme
:set guifont=Meslo\ LG\ L\ for\ Powerline:h14
" match colors with true colors when using tmux
" if exists('+termguicolors')
if (has("termguicolors"))
  let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
  let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
  set termguicolors
endif
set background=dark
hi Normal guibg=NONE ctermbg=NONE
hi RedundantSpaces ctermbg=red guibg=red
match RedundantSpaces /\s\+$/

set number relativenumber                   " show line numbers
set ruler
set ttyfast                                 " terminal acceleration

set visualbell                              " disable screen blinking
set t_vb=

set tabstop=2                               " 2 whitespaces for tabs visual presentation
set softtabstop=4
set shiftwidth=2                            " shift lines by 2 spaces
set smarttab                                " set tabs for a shifttabs logic
set expandtab                               " expand tabs into spaces
set autoindent                              " indent when moving to the next line while writing code

set cursorline                              " shows line under the cursor's line
set showmatch                               " shows matching part of bracket pairs (), [], {}

set enc=utf-8                               " utf-8 by default

set nowrap                                  " disable line wrapping

set nobackup                                " no backup files
set nowritebackup                           " only in case you don't want a backup file while editing
set noswapfile                              " no swap files

set backspace=indent,eol,start              " backspace removes all (indents, EOLs, start) What is start?

set scrolloff=10                            " let 10 lines before/after cursor during scroll

if !has('nvim')
  set clipboard=autoselect                    " use system clipboard
endif

set exrc                                    " enable usage of additional .vimrc files from working directory
set secure                                  " prohibit .vimrc files to execute shell, create files, etc...

set undofile                                " Maintain undo history between sessions
if !has('nvim')
  set undodir=~/.vim/undodir                " https://jovicailic.org/2017/04/vim-persistent-undo/
else
  set undodir=~/.config/nvim/undodir
endif

set colorcolumn=100                          " set visual line length indicator

" stolen from: https://stackoverflow.com/questions/19330843/how-do-i-
" automatically-load-a-tag-file-from-a-directory-when-changing-to-that-di
set tags=./tags;,tags;$HOME                 " seach for tags recuresevly upwards

" https://vim.fandom.com/wiki/Great_wildmode/wildmenu_and_console_mouse
set wildmenu
set wildmode=list:longest,full              " greatest wildmenu
set pastetoggle=<F2>

" visualize tabs and trailing whitespaces
set listchars=tab:>·,trail:~,extends:>,precedes:<
set list

"=====================================================
"" Keybindings
"=====================================================
let mapleader = ","
map ; :

" open & load vimrc
nnoremap <leader>rc :e $MYVIMRC<CR>
nnoremap <leader>lrc :so $MYVIMRC<CR> :echo $MYVIMRC." loaded"<CR>
" window movement
nnoremap <silent> <c-j> :wincmd j<CR>
nnoremap <silent> <c-k> :wincmd k<CR>
nnoremap <silent> <c-h> :wincmd h<CR>
nnoremap <silent> <c-l> :wincmd l<CR>
" show tabs and spaces
nnoremap <leader>st :set list!<CR>
" insert date
nmap <silent> <leader>id :.!date "+<\%Y-\%m-\%d>"<CR>
nnoremap <silent> <leader>g :Goyo <CR>
nnoremap <silent> <leader>pi :so $MYVIMRC<CR> :PluginInstall <CR>
nnoremap <silent> <leader>pc :PluginClean <CR>
nnoremap <silent> <leader>pu :PluginUpdate <CR>
" fzf binds
nnoremap <silent> <leader>ff :Files <CR>
nnoremap <silent> <leader>fg :GFiles <CR>
nnoremap <silent> <leader>fG :GFiles? <CR>
nnoremap <silent> <leader>b :Buffers <CR>
nnoremap <silent> <leader>fl :BLines <CR>
nnoremap <silent> <leader>f; :History: <CR>
nnoremap <silent> <leader>f/ :History/ <CR>
nnoremap <silent> <leader>fs :Snippets <CR>
nnoremap <silent> <leader>fh :Helptags <CR>
nnoremap <silent> <leader>ft :Tags <CR>
nnoremap <leader>wr :vertical resize 83<CR> :echo "Fit text on screen"<CR>
" open current directory
nnoremap <silent> <leader>d :e . <CR>

" taken from https://vim.fandom.com/wiki/Copy_and_paste_between_sessions_using_a_temporary_file
vmap <leader>c "*y     " Yank current selection into system clipboard
nmap <leader>C "*Y     " Yank current line into system clipboard (if nothing is selected)
nmap <leader>v "*p     " Paste from system clipboard

" use xclip to copy between vim sessions
nmap <leader>yy :w ! xclip<CR>
nmap <leader>pp :r ! xclip -o<CR>

""" spell checker options
hi clear SpellBad
hi SpellBad cterm=undercurl,bold ctermfg=red
""" open spell file
nnoremap <leader>os :e ~/.vim/spell <CR>
""" toggle spell mode
nnoremap <leader>ls :set spell! <CR>

"=====================================================
"" Search settings
"=====================================================
set incsearch                             " incremental search
set hlsearch                              " highlight search results
" disable highlighting till next search
nmap <silent> <space> :nohlsearch<CR>

"=====================================================
"" AirLine settings
"=====================================================
:let g:airline_extensions = []
let g:airline_theme='minimalist'
let g:airline_powerline_fonts=1

"=====================================================
"" NERDcommenter settings
"=====================================================
" required
filetype plugin on
" Add spaces after comment delimiters by default
let g:NERDSpaceDelims = 1
" Use compact syntax for prettified multi-line comments
let g:NERDCompactSexyComs = 1
" Align line-wise comment delimiters flush left instead of following code indentation
let g:NERDDefaultAlign = 'left'
" Set a language to use its alternate delimiters by default
let g:NERDAltDelims_java = 1
" Add your own custom formats or override the defaults
" TODO add differen comment styles for c
let g:NERDCustomDelimiters = {
  \ 'c':     { 'left': '/**', 'right': '*/' },
  \ 'julia': { 'left': '#',   'right': '' },
  \ 'par':   { 'left': '#',   'right': '' },
  \ 'vimrc': { 'left': '"',   'right': '' },
  \ 'gp':    { 'left': '#',   'right': '' }
\ }
" Allow commenting and inverting empty lines (useful when commenting a region)
let g:NERDCommentEmptyLines = 1
" Enable trimming of trailing whitespace when uncommenting
let g:NERDTrimTrailingWhitespace = 1
" Enable NERDCommenterToggle to check all selected lines is commented or not
let g:NERDToggleCheckAllLines = 1

"=====================================================
"" Polyglot settings
"=====================================================
" treat .m files as mathematica files not matlab
let filetype_m = "mma"
augroup filetypedetect
au BufRead,BufNewFile *.math set filetype=mma
augroup END

"=====================================================
"" ultisnips settings
"=====================================================
let g:UltiSnipsEditSplit="context"
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsListSnippets="<c-s>"
let g:UltiSnipsJumpForwardTrigger="<tab>"
let g:UltiSnipsJumpBackwardTrigger="<s-tab>"

"=====================================================
"" fzf settings
"=====================================================
" [Buffers] Jump to the existing window if possible
let g:fzf_buffers_jump = 1

"=====================================================
"" vim-checkbox settings
"=====================================================
let g:checkbox_states = [' ', '-', 'x' ]

"=====================================================
"" vim-easy-align settings
"=====================================================
vmap <Enter> <Plug>(EasyAlign)
nmap ga <Plug>(EasyAlign)

"=====================================================
"" vimtex settings
"=====================================================
set fillchars=fold:\
highlight Folded guifg=#98c379
let g:tex_flavor = 'latex'
let g:tex_fast = ""
let g:tex_conceal = ""
let g:vimtex_indent_enabled = 0
let g:vimtex_quickfix_open_on_warning = 0
let g:vimtex_view_automatic = 1
let g:vimtex_view_skim_reading_bar = 0
let g:vimtex_fold_enabled = 0
let g:vimtex_fold_manual = 1
let g:vimtex_view_method = 'zathura'
let g:vimtex_view_general_viewer = 'evince'
let g:vimtex_fold_types = {
  \ 'preamble' : {'enabled' : 0},
  \ 'envs' : {
  \   'blacklist' : ['figure', 'table'],
  \   'sections' : {
  \     'parse_levels' : 0,
  \     'sections' : [
  \       '%(add)?part',
  \       '%(chapter|addchap)',
  \       '%(section|addsec)',
  \       'subsection',
  \       'subsubsection',
  \       'paragraph',
  \     ],
  \     'parts' : [
  \       'appendix',
  \       'frontmatter',
  \       'mainmatter',
  \       'backmatter',
  \     ],
  \   }
  \ }
  \}
let g:vimtex_compiler_latexmk = {
  \ 'build_dir' : '',
  \ 'callback' : 1,
  \ 'continuous' : 1,
  \ 'executable' : 'latexmk',
  \ 'hooks' : [],
  \ 'options' : [
  \   '-verbose',
  \   '-file-line-error',
  \   '-synctex=1',
  \   '-interaction=nonstopmode',
  \ ],
  \}

"=====================================================
"" vim-slime settings
"=====================================================
let g:slime_target = "tmux"

"=====================================================
"" sneak settings
"=====================================================
let g:sneak#label = 1
let g:sneak#s_next = 1

" enable latex-to-unicode mapping from julia-vim for all file types
let g:latex_to_unicode_file_types = ".*"
let g:latex_to_unicode_file_types_blacklist = ["tex"]
