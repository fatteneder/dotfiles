#!/usr/bin/env bash

# download and install vundle
vimdir="$HOME/.vim"
if [ -e $vimdir ]
then
    rm -rf $vimdir
fi
# git clone "https://github.com/VundleVim/Vundle.vim.git" ~/.vim/bundle/Vundle.vim
git clone "https://github.com/VundleVim/Vundle.vim.git" "$vimdir/bundle/Vundle.vim"
# download vundle pulgins
vim +PluginInstall +qall
# generate undo dir
mkdir "$vimdir/undodir"
