#!/usr/bin/env bash

# Decompress all webp files into PNG images.

path="$HOME/wallpapers/"
webps=($(find $path -maxdepth 1 -regex '.*webp'))

for webp in "${webps[@]}"; do
  png="${webp/webp/png}"
  if [ ! -f $png ]; then
    dwebp "$webp" -o "$png"
  fi
done
