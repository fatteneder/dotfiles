#!/usr/bin/env zsh

# setup some environment variables
# not sure why it needs :1 and not :0 on fwork
if [[ $(hostname) == "fwork" ]]; then
  export DISPLAY=:1
fi
# export XAUTHORITY="$HOME/.Xauthority"

# select a random wallpaper
wallpaperpath="$HOME/wallpapers/"
wallpapers=($(find "$wallpaperpath" -type f -regex '.*\(jpg\|png\)' | sort -R ))
wp="${wallpapers[1]}"

# query active displays
outputs=$(xrandr --listmonitors | grep '+' | awk {'print $4'})

logfile="/tmp/wallpaper.shuffle.log"
echo "" >> "$logfile"
echo "================================================================" >> "$logfile"
printf "Shuffling wallpapers at $(date)\n" >> "$logfile"
for output in "${outputs[@]}"; do
  xwallpaper --debug --output "$output" --zoom "$wp" >> "$logfile" 2>&1
done
