#!/usr/bin/env bash


# download gnuplot files
gnuplotdir="$HOME/.gnuplot-cfg"
echo $gnuplotdir
if [ -e $gnuplotdir ]
then
    rm -rf $gnuplotdir
fi
git clone "https://github.com/Gnuplotting/gnuplot-palettes" "$gnuplotdir/palettes"
git clone "https://github.com/Gnuplotting/gnuplot-configs" "$gnuplotdir/snippets"
