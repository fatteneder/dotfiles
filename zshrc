insertpath() {
  local dir pos re
  dir=$1
  pos=$2
  re="(^$dir:|:$dir:|:$dir$)"
  if ! [[ "$PATH" =~ $re ]]; then
    if [[ "$pos" == "first" ]]; then
      PATH="$dir:$PATH"
    elif [[ "$pos" == "last" ]]; then
      PATH="$PATH:$dir"
    fi
  fi
}

addpath() {
  insertpath "$1" "first"
}

appendpath() {
  insertpath "$1" "last"
}

enable_conda() {
  # >>> conda initialize >>>
  # !! Contents within this block are managed by 'conda init' !!
  __conda_setup="$('/usr/bin/conda' 'shell.zsh' 'hook' 2> /dev/null)"
  if [ $? -eq 0 ]; then
      eval "$__conda_setup"
  else
      if [ -f "/usr/etc/profile.d/conda.sh" ]; then
          . "/usr/etc/profile.d/conda.sh"
      else
          export PATH="/usr/bin:$PATH"
      fi
  fi
  unset __conda_setup
  # <<< conda initialize <<<
}



# Path to your installations.
if [[ $(hostname) == "voidy" ]]; then
  if [ -z "${DISPLAY}" ] && [ "${XDG_VTNR}" -eq 1 ]; then
      exec startx
  fi
  export ZSH="/home/flo/.oh-my-zsh"
  export FZF_BASE="/usr/share/doc/fzf"
  # appendpath "$HOME/miniconda3/bin"
  appendpath "$HOME/dotfiles"
  appendpath "$HOME/binaries/julia-1.7.1/bin"
  appendpath "$HOME/binaries/marp-cli"

  # Still needed?
  fpath=(~/.zsh/completion $fpath)
  autoload -Uz compinit && compinit -i
elif [[ $(hostname) == "fwork" ]]; then
  export ZSH="$HOME/.oh-my-zsh"
  appendpath "$HOME/.juliaup/bin"
  appendpath "$HOME/dotfiles"
  appendpath "$HOME/.local/bin"
  appendpath "$HOME/.cargo/bin"
  appendpath "$HOME/.vim/bundle/fzf/bin"
  appendpath "$HOME/wd/v"
  export LD_PRELOAD="/usr/lib64/libstdc++.so.6"
  export LIBVA_DRIVER_NAME=iHD

  # export WORKON_HOME=$HOME/virtualenvs
  # export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3
  # . $HOME/.local/bin/virtualenvwrapper.sh

  # export JULIA_PKG_SERVER=https://staging.pkg.julialang.org
fi


#######################################################################
#                         Oh-My-Zsh settings                          #
#######################################################################

# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
ZSH_THEME="miloshadzic"
COMPLETION_WAITING_DOTS="true"
DISABLE_UNTRACKED_FILES_DIRTY="true"

plugins=(vi-mode fzf)

source $HOME/.oh-my-zsh/oh-my-zsh.sh

#######################################################################
#                         User configuration                          #
#######################################################################
#

# settings from https://superuser.com/questions/585003/searching-through-history-with-up-and-down-arrow-in-zsh
autoload -U up-line-or-beginning-search
autoload -U down-line-or-beginning-search
zle -N up-line-or-beginning-search
zle -N down-line-or-beginning-search
if [[ $(hostname) == "voidy" ]]; then
  bindkey "${terminfo[kcuu1]}" up-line-or-beginning-search # Up
  bindkey "${terminfo[kcud1]}" down-line-or-beginning-search # Down
fi

# set vim as default editor
has_nvim=$(which nvim >/dev/null && echo 1 || echo 0)
if [[ ${has_nvim} ]]; then
  export VISUAL="nvim"
  alias v="nvim"
else
  export VISUAL="vim"
  alias v="vim"
fi
export EDITOR="$VISUAL"


#=====================================================
# My Aliases
#=====================================================


alias cmj="cmake --build . --parallel"
alias vo="v -o \`fzf\`"
alias vrc="v ~/.vimrc"
alias vzsh="v ~/.zshrc"
alias szsh="source ~/.zshrc"
alias vtmux="v ~/.tmux.conf"
alias stmux="tmux source-file ~/.tmux.conf"
alias j="jobs"
alias gp="gnuplot"
alias mj="make -j"
alias rg="rg --no-heading"
alias python="python3"
alias py="python"
alias ipy="ipython"
alias gs="git status"
alias mm="neomutt"
alias g="git"
alias jl="julia"
alias cb="cargo build"
alias cr="cargo run"
alias cc="cargo check"
alias marpdf="marp --allow-local-files --pdf --jpeg-quality 100"
alias feh="feh -d"
alias marps="marp --html --theme-set ~/wd/mymarptheme -s . &"
alias lg="lazygit"
alias vbspwm="v $HOME/.config/bspwm/bspwmrc"
alias vsxkhd="v $HOME/.config/sxhkd/sxhkdrc"
alias vpicom="v $HOME/.config/picom/picom.conf"
