#!/usr/bin/env bash

## TODO
# Add commands
#   [x] link
#   [x] unlink
#   [x] help
#   [x] list
# Extend config file syntax
#   [x] allow comments and empty lines
#   [ ] allow flags
#     -i ... run script before linking
#     -p ... run script after linking
#     -s ... set softlink
#   [ ] allow alias to other config
#     perhaps with an import keyword
# Add command line options
#   [ ] -f ... force linking, force removal
#   [ ] -p ... prefix, for custom home directory location
# Test linking with directories (requires soft links)

thisdir="$(dirname $0)"
cfgfile="$thisdir/cfgs.txt"
cmd="$1"

skip_line() {
    if [[ "${1::1}" == "#" ]] || [[ "$1" == "" ]]; then
        return 0
    fi
    return 1
}

get_cfg() {
    cfg="$1"
    n_lines=$(awk 'END {print NR}' $cfgfile)
    idx_line=0
    cat $cfgfile | while read line; do
        idx_line=$((idx_line+1))
        skip_line $line && continue
        entries=($line)
        if [[ $found ]]; then
            if [[ "${entries[0]}" == "@" ]]; then
                result="${result[@]} ${entries[@]:1}"
            else
                echo "${result[@]}"
                break
            fi
        fi
        if [[ "$cfg" == "${entries[0]}" ]]; then
            result="${entries[@]:1:${#entries[@]}}"
            if [[ $idx_line == $n_lines ]]; then
                echo "${result[@]}"
            fi
            found=0
        fi
    done
}

list_cfgs() {
cat $cfgfile | while read line; do
    skip_line $line && continue
    entries=($line)
    if [[ "${entries[0]}" != "@" ]]; then
        echo "${entries[0]}"
    fi
done
}

cmd="$1"

link_cfg() {
    cfg="$1"
    echo "Linking ${cfg} ..."
    entries=($(get_cfg $cfg))
    n_entries="${#entries[@]}"
    if [[ $n_entries == 0 ]]; then
        echo "No configuration '$cfg' found"
        exit
    fi
    if [[ $(($n_entries % 2)) != 0 ]]; then
        echo "Configuration is incomplete"
        exit
    fi
    n_pairs=$(($n_entries/2))
    for ((idx=0;idx<$n_pairs;idx++)); do
        src="$thisdir/${entries[$((2*$idx))]}"
        target="$HOME/${entries[$((2*$idx+1))]}"
        targetdir=$(dirname $target)
        mkdir -p "$targetdir"
        ln -vi ${src} ${target}
    done
}

unlink_cfg() {
    cfg="$1"
    echo "Unlinking ${cfg} ..."
    entries=($(get_cfg $cfg))
    n_entries="${#entries[@]}"
    if [[ $(($n_entries % 2)) != 0 ]]; then
        echo "Configuration is incomplete"
        exit
    fi
    n_pairs=$(($n_entries/2))
    for ((idx=0;idx<$n_pairs;idx++)); do
        target="$HOME/${entries[$((2*$idx+1))]}"
        if [[ -f "$target" ]]; then
            rm -iv $target
        fi
    done
}

cmd="$1"
args=("${@:2}")


case "$cmd" in

    "link")
        for arg in "${args[@]}"; do
            link_cfg $arg
        done
        ;;

    "unlink")
        for arg in "${args[@]}"; do
            unlink_cfg $arg
        done
        ;;

    "list")
        list_cfgs
        ;;

    "help")
        echo "Config file syntax

    Configs are stored in a file cfg.txt that must be on the same path as this 
    script. 

    You can add a new config by appending a line to cfg.txt that starts with 
    a config key and is followed by pairs of filenames. 
    The first filename refers to the config source file that will
    be linked and the second one refers to the destination where the 
    source file should be linked to. The former filename is taken relative 
    to this scripts location and the latter one is preappended with the value 
    of the \$HOME environment variable. 

    Here is an example from config: 
        
        vim  vimrc  .vimrc

    This line adds a cfg entry 'vim', when combined with the link
    command dots.sh generates a hardlink of the form 

        $ ./dots.sh link vim
        Linking vim ...
        $HOME/.vimrc => vimrc

    Usually, one line corresponds to one config. If needed, one can insert a line
    break and continue adding filename pairs by starting the new line with the `@`
    char, e.g.

        neomutt    muttrc      .muttrc    mbsynrc  .mbsyncrc   mutt    .mutt   
        @          msmtprc     .msmtprc
    "
        ;;

    *)
        echo "usage: dots link <cfg>        hardlink files"
        echo "       dots unlink <cfg>      unlink files"
        echo "       dots list              list available configs"
        echo "       dots help              help about config file syntax"
        exit -1
        ;;

esac

exit 0
