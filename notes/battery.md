# Battery

TODO: List links from the Framework websites where they discuss
different tweaks for battery save mode.

IIRC the most important app is `tlp`.
But there was something about `tlp` that made it not work in parallel
with another app.

## Screen diming

When running `bspwm` and `tlp` and watching videos with `vlc`, it might
happen that the screen is automatically dimmed, even if `Screen blank`
is deactivated in `Settings -> Power`.

Check if it
```sh
gsettings get org.gnome.settings.plugin.power idle-dim
```
and turn it off if with
```sh
gsettings set org.gnome.settings.plugin.power idle-dim false
```
