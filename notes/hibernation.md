# Hibernation

Tested on Fedora 36 and Linux 5.17.7-300.fc36.x86_64

1. Setup a swap partition. For hibernation it should be RAM size plus some swap pressure.
For my 16G RAM sized laptop I added 10G on top so that I had 26G swap.

2. Follow https://docs.fedoraproject.org/en-US/quick-docs/encrypting-drives-using-LUKS/
to encrypt the swap partition.

In particular, run this to encrypt the partition
```
dd if=/dev/urandom of=<device>
cryptsetup luksFormat <device>
cryptsetup isLuks <device> && Success # check
cryptsetup luksDump <device> # print summary
cryptsetup luksUUID <device> # get UUID
cryptsetup luksOpen <device> luks-<UUID> # decrypt
```
There should now be a device called `/dev/mapper/luks-<UUID>`.

Then format partition
```
mkswap -L swap /dev/mapper/luks-<UUID>
```

Lastly, add the following entry to `/etc/fstab`:
```
/dev/mapper/luks-UUID none swap sw 0 0
```


3. Follow these instructions to configure grub to resume from the swap partition
https://www.ctrl.blog/entry/fedora-hibernate.html. Sepcifically,
- add `add_dracutmodules+=" resume "` to `/etc/dracut.conf` and reconfigure initramfs with
`dracut -f`,
- edit `/etc/default/grub` and append `resume=/dev/mapper/<name> rd.luks.uuid=<name>` to
`GRUB_CMDLINE_LINUX` and then regenerate the grub config with
`grub2-mkconfig -o /boot/efi/EFI/fedora/grub.conf`; note that the blog post says to not duplicate
'some' parameter, but for me `rd.luks.uuid` was already from the luks home partition
and leaving it out for the swap partition did not let me boot again,
- update `/etc/systemd/sleep.conf` and enable set options
```
[Sleep]
AllowHibernation=yes
HibernateMode=shutdown
```

4. According to the Framework forums one should use sleep-then-hibernate mode when
closing the lid. See this blog for instructions to update `/etc/systemd/login.conf` and
`/etc/systemd/sleep.conf`
https://www.jameskupke.com/posts/using-framework-with-fedora/#enabling-hibernation


## Troubleshooting

- 31.12.2022: Problem on boot where /var/log/boot.log (or journalctl -b | grep crypt) said
reported something like `failed to start systemd-cryptsetup@swap.service`.
Checking status with `systemctl status systemd-cryptsetup@swap.service` said something abouot
trying to map or mount a device that is busy (already mapped/mounted).
Solution to this was to add the right entry to `/etc/fstab`, see above.

