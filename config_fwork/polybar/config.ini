;==========================================================
;
;
;   ██████╗  ██████╗ ██╗  ██╗   ██╗██████╗  █████╗ ██████╗
;   ██╔══██╗██╔═══██╗██║  ╚██╗ ██╔╝██╔══██╗██╔══██╗██╔══██╗
;   ██████╔╝██║   ██║██║   ╚████╔╝ ██████╔╝███████║██████╔╝
;   ██╔═══╝ ██║   ██║██║    ╚██╔╝  ██╔══██╗██╔══██║██╔══██╗
;   ██║     ╚██████╔╝███████╗██║   ██████╔╝██║  ██║██║  ██║
;   ╚═╝      ╚═════╝ ╚══════╝╚═╝   ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝
;
;
;   To learn more about how to configure Polybar
;   go to https://github.com/polybar/polybar
;
;   The README contains a lot of information
;
;==========================================================

[colors]
background = #222
foreground = #dfdfdf
foreground-alt = #555
primary = #ffb52a
secondary = #e60053
alert = #bd2c40

[bar/bar1]
monitor = ${env:MONITOR:eDP-1}
width = 100%
height = 40
radius = 0.0
fixed-center = false

background = ${colors.background}
foreground = ${colors.foreground}

line-size = 3
line-color = #f00

border-size = 0
border-color = #00000000

padding-left = 0
padding-right = 2

module-margin-left = 2
module-margin-right = 2

font-0 = Bitstream Vera Sans Roman:pixelsize=14;1
font-1 = Twitter Color Emoji:scale=6;1
font-2 = JuliaMono:pixelsize=14;1
font-3 = Siji:pixelsize=10;1

modules-left = setxkbmap date bspwm
; modules-center = date
modules-right = pulseaudio backlight wireless-network battery powermenu

tray-position = right
separator = |

cursor-click = pointer
cursor-scroll = ns-resize

bottom = true

enable-ipc = true

; override-redirect = true
wm-restack = bspwm

[module/xwindow]
type = internal/xwindow
label = %title%

[module/filesystem]
type = internal/fs
interval = 25

mount-0 = /
mount-1 = /home

label-mounted = %{F#0a81f5}%mountpoint%%{F-}: %percentage_used%%
label-unmounted = %mountpoint% not mounted
label-unmounted-foreground = ${colors.foreground-alt}

[module/bspwm]
type = internal/bspwm

label-focused = %index%
label-focused-underline= ${colors.primary}
label-focused-padding = 2

label-occupied = %index%
label-occupied-padding = 2

label-urgent = %index%!
label-urgent-background = ${colors.alert}
label-urgent-padding = 2

label-empty = %index%
label-empty-foreground = ${colors.foreground-alt}
label-empty-padding = 2

[module/backlight]
type = internal/backlight
card = intel_backlight
use-actual-birghtness = true
enable-scroll = true

format = <ramp> <label> <bar>
label = %percentage%%
; format-underline = #9f78e1

bar-width = 10
bar-indicator = |
bar-indicator-foreground = fff
bar-fill-foreground = #9f78e1
bar-empty-foreground = ${colors.foreground-alt}
bar-indicator-font = 7
bar-fill = ─
bar-fill-font = 5
bar-empty = ─
bar-empty-font = 5

ramp-0 = "🌑"
ramp-1 = "🌒"
ramp-2 = "🌓"
ramp-3 = "🌔"
ramp-4 = "🌕"

[module/cpu]
type = internal/cpu
interval = 10
format-prefix = " "
; format-underline = #8fce00
label = %percentage:2%%

[module/memory]
type = internal/memory
interval = 10
format-prefix = " "
; format-underline = #4bffdc
label = %percentage_used%%

[module/date]
type = internal/date
interval = 5
date = " %d-%m-%Y"
time = "%H:%M"
format = 📆<label> 🕙
label = %date%, %time% 
; format-underline = #15538c
label-padding = 10px
click-left = /usr/bin/gnome-calendar

[module/pulseaudio]
type = internal/pulseaudio

format-volume = <ramp-volume> <label-volume> <bar-volume>
label-volume = %percentage%%
label-volume-foreground = ${root.foreground}
; format-volume-underline = #55aa55

interval = 10

label-muted = sound muted
label-muted-foreground = #666

bar-volume-width = 10
bar-volume-foreground-0 = #55aa55
bar-volume-foreground-1 = #55aa55
bar-volume-foreground-2 = #55aa55
bar-volume-foreground-3 = #55aa55
bar-volume-foreground-4 = #55aa55
bar-volume-foreground-5 = #f5a70a
bar-volume-foreground-6 = #ff5555
bar-volume-gradient = false
bar-volume-indicator = |
bar-volume-indicator-font = 7
bar-volume-fill = ─
bar-volume-fill-font = 5
bar-volume-empty = ─
bar-volume-empty-font = 5
bar-volume-empty-foreground = ${colors.foreground-alt}

ramp-volume-0 = "🔈"
ramp-volume-1 = "🔈"
ramp-volume-2 = "🔉"
ramp-volume-3 = "🔉"
ramp-volume-4 = "🔊"
ramp-volume-5 = "🔊"

[module/temperature]
type = internal/temperature
thermal-zone = 0
warn-temperature = 60

format = <ramp> <label>
; format-underline = #e84803
format-warn = <ramp> <label-warn>
; format-warn-underline = ${self.format-underline}

label = %temperature-c%
label-warn = %temperature-c%
label-warn-foreground = #f10000

ramp-0 =🌡

[module/setxkbmap]
type = custom/script
exec = $HOME/.config/polybar/setxkbmap.sh
click-left = $HOME/.config/polybar/setxkbmap.sh --toggle &
interval = 3

[module/powermenu]
type = custom/menu

expand-right = true

format-spacing = 1

label-open = 💤
label-open-foreground = ${colors.secondary}
label-close =  cancel
label-close-foreground = ${colors.secondary}
label-separator = |
label-separator-foreground = ${colors.foreground-alt}

menu-0-0 = reboot
menu-0-0-exec = menu-open-1
menu-0-1 = power off
menu-0-1-exec = menu-open-2
menu-0-2 = suspend-then-hibernate
menu-0-2-exec = menu-open-3

menu-1-0 = cancel
menu-1-0-exec = menu-open-0
menu-1-1 = reboot
menu-1-1-exec = reboot

menu-2-0 = power off
menu-2-0-exec = poweroff
menu-2-1 = cancel
menu-2-1-exec = menu-open-0

menu-3-0 = suspend-then-hibernate
menu-3-0-exec = systemctl suspend-then-hibernate
menu-3-1 = cancel
menu-3-1-exec = menu-open-0

[module/battery]
type          = internal/battery
full-at       = 99
low-at        = 10
battery       = BAT1
adapter       = ACAD
poll-interval = 5

format-charging            = 🔌⚡ <label-charging>
format-discharging         = ⚡ <label-discharging>
; format-full                = ⚡ <label-charging>
; format-low                 = ⚡ <label-discharging>
format-charging-padding    = 10px
format-discharging-padding = 10px
format-full-padding        = 10px
format-low-padding         = 10px
format-discharging-suffix  = " "
format-charging-suffix     = " "
format-full-suffix         = " "
format-low-suffix          = " "

; format-discharging-underline = #fcca0b
; format-charging-underline    = #55aa55
; format-full-underline        = #fcca0b
; format-low-underline         = #fcca0b
format-low-background        = #d62d20

label-charging    = %percentage%%
label-discharging = %percentage%%
label-full        = %percentage%%
label-low         = %percentage%%

[settings]
screenchange-reload = true

[global/wm]
margin-top = 0
margin-bottom = 5

[module/wireless-network]
type = internal/network
interface = wlp170s0
interface-type = wireless

interval = 3.0
ping-interval = 0

format-connected = <ramp-signal> <label-connected>
format-packetloss = <ramp-signal> <label-connected>
; format-connected-underline = #f12345
; format-disconnected-underline = #f12345
; format-packetloss-underline = #f12345

label-disconnected = %{A1:gnome-control-center wifi:}%{A3:$HOME/.config/polybar/wifitoggle.sh:}💀%{A}%{A}
label-connected = %{A1:gnome-control-center wifi:}%{A3:$HOME/.config/polybar/wifitoggle.sh:}%essid%%{A}%{A}

ramp-signal-0 = "🙀"
ramp-signal-1 = "🙀"
ramp-signal-2 = "😿"
ramp-signal-3 = "😸"
ramp-signal-4 = "😻"
ramp-signal-5 = "😻"

[module/bluetoothctl]
type = custom/script
exec = $HOME/.config/polybar/bluetoothctl.sh
tail = true
click-left = gnome-control-center bluetooth
click-right = $HOME/.config/polybar/bluetoothctl.sh --toggle &
