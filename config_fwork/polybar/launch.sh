#!/usr/bin sh

killall -q polybar

echo "---" | tee -a /tmp/polybar.log
polybar bar1 2>&1 | tee -a /tmp/polybar.log & disown

echo "Polybar launched ..."
