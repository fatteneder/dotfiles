#!/bin/sh

layout() {
    echo $(setxkbmap -query | grep layout | grep us)
}

case "$1" in
    --toggle)
        if [[ ! -z $(layout) ]]; then
            setxkbmap de
        else
            setxkbmap us
        fi
        ;;
    *)
        if [[ ! -z $(layout) ]]; then
            echo "   us"
        else
            echo "   de"
        fi
        ;;
esac
