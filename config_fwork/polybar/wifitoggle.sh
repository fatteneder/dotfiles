#!/bin/sh

if nmcli radio wifi | grep -q "enabled"; then
    nmcli radio wifi off >> /dev/null
else
    nmcli radio wifi on >> /dev/null
fi
