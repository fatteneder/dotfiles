if [[ $(hostname -d) == "ara" ]]; then
  export TERM="xterm-256color"
fi
if [ -f "$HOME/.bashrc" ]; then
  source "$HOME/.bashrc"
fi
