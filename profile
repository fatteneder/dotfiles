if [ -d "$HOME/.local/bin" ]; then
  PATH="$HOME/.local/bin:$PATH"
fi
if [[ -f "$HOME/.xinput" && "$XDG_SESSION_TYPE" = "x11" ]]; then
  "$HOME/.xinput"
fi
