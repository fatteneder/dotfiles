#######################################################################
#                         Auxiliary functions                         #
#######################################################################

insertpath() {
  local dir pos re
  dir=$1
  pos=$2
  re="(^$dir:|:$dir:|:$dir$)"
  if ! [[ "$PATH" =~ $re ]]; then
    if [[ "$pos" == "first" ]]; then
      PATH="$dir:$PATH"
    elif [[ "$pos" == "last" ]]; then
      PATH="$PATH:$dir"
    fi
  fi
}

addpath() {
  insertpath "$1" "first"
}

appendpath() {
  insertpath "$1" "last"
}

enable_conda() {
  # >>> conda initialize >>>
  # !! Contents within this block are managed by 'conda init' !!
  __conda_setup="$('/ssd/du23lag/miniconda3/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
  if [ $? -eq 0 ]; then
      eval "$__conda_setup"
  else
      if [ -f "/ssd/du23lag/miniconda3/etc/profile.d/conda.sh" ]; then
          . "/ssd/du23lag/miniconda3/etc/profile.d/conda.sh"
      else
          export PATH="/ssd/du23lag/miniconda3/bin:$PATH"
      fi
  fi
  unset __conda_setup
  # <<< conda initialize <<<
}

#######################################################################
#                         User configuration                          #
#######################################################################

# enable vim mode
set -o vi

# make vim standard editor
export EDITOR="/usr/bin/vim"
export VISUAL="/usr/bin/vim"

# change prompt
export PS1="[\[\e[1;37m\]\[\e[1;95m\]\h\[\e[0;39m\]:\[\e[1;37m\]\[\e[1;32m\]\u\[\e[0;39m\]:\[\e[1;33m\]\W\[\e[0;39m\]\[\e[0;39m\]] "

# suppress warnings (but which ones?)
unset LC_CTYPE

# GPG config
export GPG_TTY="tty"
export GPG_AGENT_INFO="gpgconf --list-dirs agent-socket | tr -d '\n' && echo -n ::"

# display terminal freeze which happens when pressing Ctrl+S (unfreeze is done with Ctrl+Q)
# below option disables that
stty -ixon

# setup clean PATH
export PATH="$(getconf PATH)"
appendpath "~/dotfiles/"
if [[ $(hostname -d) == "ara" ]]; then
  export http_proxy="http://internet4nzm.rz.uni-jena.de:3128"
  export https_proxy="http://internet4nzm.rz.uni-jena.de:3128"
elif [[ $(hostname) == "pc2011" ]]; then
  appendpath "$HOME/.local/bin"
  appendpath "$HOME/.julia/juliaup/bin"
  appendpath "/ssd/du23lag/muninn/target/release"
  appendpath "/ssd/du23lag/cargo/bin"
  appendpath "/usr/local/bin"
  addpath "/ssd/du23lag/miniconda3/bin"
  export RUSTUP_HOME="/ssd/du23lag/rustup"
  export CARGO_HOME="/ssd/du23lag/cargo"
elif [[ $(hostname) == "pc2104" ]]; then
  appendpath "/ssd2/du23lag/muninn/target/release"
  export RUSTUP_HOME="/ssd2/du23lag/rustup"
  export CARGO_HOME="/ssd2/du23lag/cargo"
  appendpath "$RUSTUP_HOME/bin"
  appendpath "$CARGO_HOME/bin"
fi
appendpath "$HOME/wd/ahloc3d"
export PATH


#######################################################################
#                               Aliases                               #
#######################################################################


alias gdb="gdb -tui"
alias diff="/bin/diff"
alias supdiff="diff --suppress-common-lines"
alias grep="grep --color=auto"
alias mj="make -j"
alias m="make"
alias j="jobs"
alias ls="ls --color=auto"
alias v="vim"
alias vo="vim -o \`fzf\`"
alias vrc="vim ~/.vimrc"
alias szsh="source ~/.zshrc"
alias tmux="tmux -2"
alias stmux="tmux source-file ~/.tmux.conf"
alias sbash="source ~/.bashrc"
alias j="jobs"
alias gp="gnuplot"
alias mj="make -j"
alias rg="rg --no-heading"
#alias python="python3"
alias py="python"
alias ipy="ipython"
alias gs="git status"
alias mm="neomutt"
alias g="git"
alias jl="julia --color=yes -O0"
alias jlo="julia --color=yes -O3"
alias vbash="vim ~/.bashrc"
alias vvim="vim ~/.vimrc"
alias vtmux="vim ~/.tmux.conf"
alias python3="/usr/bin/python3"
alias marp="npx marp"
alias marpdf="marp --allow-local-files --pdf --jpeg-quality 100"
alias marps="marp --html --theme-set ~/wd/mymarptheme -s . &"
alias hn="hn"
alias myqueue="squeue -u \"$(whoami)\""
alias mydetsqueue='squeue --format="%.18i %.9P %.50j %.8u %.8T %.10M %.10S %.6D %R" | grep du23lag'

# fuzzy search
[ -f ~/.fzf.bash ] && source ~/.fzf.bash
