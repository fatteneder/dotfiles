set macros
set encoding utf8
set samples 1000
set isosamples 75
set hidden3d
set terminal qt font "Arial"

# automatically iterates over line styles instead of line types in the plot command
# https://stackoverflow.com/questions/22556066/gnuplot-multiple-plot-files-using-the-same-style-settings-but-different-numbe
set style increment user 

homedir=system("echo $HOME")
set loadpath    homedir."/.gnuplot-cfg/palettes"\
                homedir."/.gnuplot-cfg/snippets"\
                homedir."/gnuplot-fns"\
                homedir."/wd/skimplot"
undefine homedir


Grid = "load 'grid.cfg'"
Xyborder = "load 'xyborder.cfg'"
Dark2 = "load 'dark2.pal'"
Viridis = "load 'viridis.pal'"
Plasma = "load 'plasma.pal'"
Parula = "load 'parula.pal'"
Paired = "load 'paired.pal'"
Set1 = "load 'set1.pal'"
Key = "set key auto columnheader"
