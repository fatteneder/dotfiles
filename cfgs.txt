vim                 vimrc                       .vimrc
@                   UltiSnips                   .vim/UltiSnips

neomutt             muttrc                      .muttrc
@                   mbsynrc                     .mbsyncrc
@                   muttrc                      .muttrc
@                   msmtprc                     .msmtprc

tmux                tmux.conf                   .tmux.conf

bash                bashrc                      .bashrc
@                   bash_profile                .bash_profile

gnuplot             gnuplot                     gnuplot

git                 gitconfig                   .gitconfig

zsh                 zshrc                       .zshrc
@                   zshenv                      .zshenv
@                   profile                     .profile

alacritty.voidy     alacritty.toml              .alacritty.toml
alacritty.fwork     alacritty.toml              .alacritty.toml

bspwm.voidy         config_voidy/bspwm/bspwmrc                     .config/bspwm/bspwmrc
sxhkd.voidy         config_voidy/sxhkd/sxhkdrc                     .config/sxhkd/sxhkdrc
picom.voidy         config_voidy/picom/picom.conf                  .config/picom/picom.conf
polybar.voidy       config_voidy/polybar/config                    .config/polybar/config
@                   config_voidy/polybar/launch.sh                 .config/polybar/launch.sh

polybar.fwork       config_fwork/polybar/config.ini                .config/polybar/config.ini
@                   config_fwork/polybar/launch.sh                 .config/polybar/launch.sh
@                   config_fwork/polybar/bluetoothctl.sh           .config/polybar/bluetoothctl.sh
@                   config_fwork/polybar/setxkbmap.sh              .config/polybar/setxkbmap.sh
@                   config_fwork/polybar/wifitoggle.sh             .config/polybar/wifitoggle.sh
rofi.fwork          config_fwork/rofi/config.rasi                  .config/rofi/config.rasi
@                   config_fwork/rofi/sidebar.rasi                 .config/rofi/sidebar.rasi
bspwm.fwork         config_fwork/bspwm/bspwmrc                     .config/bspwm/bspwmrc
sxhkd.fwork         config_fwork/sxhkd/sxhkdrc                     .config/sxhkd/sxhkdrc
@                   config_fwork/sxhkd/toggle-dunst                .config/sxhkd/toggle-dunst
picom.fwork         config_fwork/picom/picom.conf                  .config/picom/picom.conf
dunst.fwork         config_fwork/dunst/dunstrc                     .config/dunst/dunstrc
rofi-streamlink.fwork local_fwork/bin/rofi-streamlink .local/bin/rofi-streamlink
@                     local_fwork/bin/islive.py       .local/bin/islive.py

Xresources          Xresources      .Xresources

dummy
